// variables
const PAGE = $("body").attr("data-page");
var $header_top = $(".header-top");
var $nav = $("nav");
// toggle menu
// $(".toggle-menu").on("click", function () {
//   $(this).toggleClass("open-menu");
//   $(".menu-mb").toggleClass("active");
// });

// change backgound header
let header = document.querySelector(".header-top");
let slider = document.querySelector(".slider");
let heightHeader = header.clientHeight;
// let heightSlider = slider.clientHeight;

function changeBgHeader() {
    let scrollY = window.pageYOffset;

    if (scrollY > heightSlider - heightHeader) {
        header.classList.add("bg-header");
    } else {
        header.classList.remove("bg-header");
    }
}

//backtotop
let support = $(".support-online");
let heightTop = $(".bttop").innerHeight();

function showBackToTop() {
    let scrollY = window.pageYOffset;
    if (scrollY >= heightTop) {
        support.addClass("active");
    } else {
        support.removeClass("active");
    }
}
$("a.backtotop").on("click", function(e) {
    e.preventDefault();
    window.scrollTo({
        top: 0,
    });
});

// call function when scroll page
document.addEventListener("scroll", function() {
    changeBgHeader();
    showBackToTop();
});

$(".toggle-menu").on("click", function() {
    if ($(this).hasClass("open-menu")) {
        $(this).removeClass("open-menu");
        $(".menu-mb").removeClass("active");
        $("body").css("overflow-y", "auto");
    } else {
        $(this).addClass("open-menu");
        $(".menu-mb").addClass("active");
        $("body").css("overflow-y", "hidden");
    }
});

//active link when click menu item header to section
let menus = $("#menu .menu-header__item a:not(.other-page)");
let menusMB = $(
    ".menu-mb ul li.menu-header__item a.menu-header__item-link:not(.video-link)"
);
let sections = $("main section");

function removeActiveMenu(classActive) {
    menus.removeClass(classActive);
    menusMB.removeClass(classActive);
}

if (PAGE === "home") {
    // active link when click menu item header to section
    menus.on("click", function(e) {
        e.preventDefault();
        let classNameItem = $(this).attr("href").replace("#", "");
        let sectionItem = $(`.${classNameItem}`);
        let offsetSectionItem = sectionItem.offset();
        window.scrollTo({
            top: offsetSectionItem.top - heightHeader + 1,
        });
        removeActiveMenu("active");
        $(this).addClass("active");
    });

    menusMB.on("click", function(e) {
        e.preventDefault();
        if ($(this).hasClass("scroll-top")) {
            window.scrollTo({
                top: 0,
            });
            removeActiveMenu("active");
            $(this).addClass("active");
        } else {
            let classNameItemMb = $(this).attr("href").replace("#", "");
            let sectionItemMb = $(`.${classNameItemMb}`);
            let offsetSectionItemMb = sectionItemMb.offset();
            console.log(offsetSectionItemMb);
            window.scrollTo({
                top: offsetSectionItemMb.top - heightHeader + 10,
            });
            removeActiveMenu("active");
            $(this).addClass("active");
        }
        $(".menu-mb").removeClass("active");
        $(".toggle-menu").removeClass("open-menu");
    });
}
//active link when scroll menu item header to section
$(window).on("scroll", function() {
    $(".menu-mb").removeClass("active");
    $(".toggle-menu").removeClass("open-menu");
    let scrollY = window.pageYOffset;
    sections.each(function(index, section) {
        let offsetSection = $(section).offset();
        if (
            scrollY > offsetSection.top - heightHeader &&
            scrollY < offsetSection.top + section.offsetHeight
        ) {
            removeActiveMenu("active");
            // $(menus[index]).addClass("active");
            $(menusMB[index]).addClass("active");
            $('#menu a[href="#' + $(section).attr("id") + '"]').addClass("active");
        } else {
            $(menus[index]).removeClass("active");
            $(menusMB[index]).removeClass("active");
        }
    });
});

function testimonialSlider() {
    if ($(".section-testimonial").length) {
        var $carousel = $(".section-testimonial .images .list").flickity({
            contain: true,
            wrapAround: false,
            freeScroll: false,
            cellAlign: "center",
            lazyLoad: 2,
            imagesLoaded: true,
            prevNextButtons: false,
            on: {
                change: function(index) {
                    $(".testimonial .ct").removeClass("active");
                    $(".testimonial .ct-" + (index + 1)).addClass("active");
                },
            },
        });
        var flkty = $carousel.data("flickity");
        var $imgs = $(".section-testimonial .carousel-cell img");

        $carousel.on("scroll.flickity", function(event, progress) {
            flkty.slides.forEach(function(slide, i) {
                var img = $imgs[i];
                var x = ((slide.target + flkty.x) * -1) / 2;
                img.style.transform = "translateX( " + x + "px)";
            });
        });

        let ctrPrevTes = $(".section-testimonial .btn_ctr.prev"),
            ctrNextTes = $(".section-testimonial .btn_ctr.next");

        ctrPrevTes.on("click", function() {
            $carousel.flickity("previous", true);
        });
        ctrNextTes.on("click", function() {
            $carousel.flickity("next", true);
        });
    }
}
testimonialSlider();

//
let options = $(".carousel").flickity({
    initialIndex: 0,
    autoPlay: true,
    accessibility: true,
    setGallerySize: true,
    resize: true,
    cellAlign: "center",
    contain: true,
    percentPosition: true,
    prevNextButtons: true,
    pageDots: true,
    arrowShape: {
        x0: 10,
        x1: 60,
        y1: 50,
        x2: 70,
        y2: 40,
        x3: 30,
    },
});
if (matchMedia("screen and (min-width: 1200px)").matches) {
    options.cellAlign = "left";
    options.autoPlay = true;
}
//
let optionQuotes = $(".carousel-quotes").flickity({
    initialIndex: 0,
    autoPlay: true,
    accessibility: true,
    setGallerySize: true,
    resize: true,
    cellAlign: "center",
    contain: true,
    percentPosition: true,
    prevNextButtons: true,
    pageDots: true,
    arrowShape: {
        x0: 10,
        x1: 60,
        y1: 50,
        x2: 70,
        y2: 40,
        x3: 30,
    },
});
if (matchMedia("screen and (min-width: 1200px)").matches) {
    optionQuotes.cellAlign = "left";
    optionQuotes.autoPlay = true;
}

// $(window).resize(function () {
//   location.reload();
// });

$(document).ready(function() {
    $(".support-content").hide();
    $("a.btn-support").click(function(e) {
        e.stopPropagation();
        $(".support-content").slideToggle();
    });
    $(".support-content").click(function(e) {
        e.stopPropagation();
    });
    $(document).click(function() {
        $(".support-content").slideUp();
    });

});

//